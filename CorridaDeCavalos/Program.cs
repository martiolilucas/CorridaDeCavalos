﻿using System;
using System.Threading;

namespace CorridaDeCavalos
{
    class Program
    {
        static void Main(string[] args)
        {
            ////////Parametros de Execução
            Corrida.DistanciaMaximaEntreCavalos = 10;
            Corrida.TamanhoDaCorrida = 100;
            var quantidadeDeCavalos = 15;
            /////////////

            var posicaoCavalos = new int[quantidadeDeCavalos];

            Console.WriteLine("Vamos começar a corrida, aqui os competidores:");

            Thread[] threads = new Thread[quantidadeDeCavalos];
            for (var i = 0; i < quantidadeDeCavalos; i++)
            {
                var competidor = new Cavalo();
                var corrida = new Corrida(posicaoCavalos, competidor);

                Console.WriteLine($"{competidor.Numero} -> velocidade = {competidor.Velocidade}");

                threads[i] = new Thread(new ThreadStart(corrida.QueComeceACorrida));
            }

            Console.WriteLine("-------------");

            foreach (var t in threads)
                t.Start();

            foreach (var t in threads)
                t.Join();

            Console.ReadKey();

        }
    }
}
