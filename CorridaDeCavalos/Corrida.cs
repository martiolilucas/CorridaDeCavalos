﻿using System;
using System.Threading;

namespace CorridaDeCavalos
{
    class Corrida
    {
        private static int _colocacao = 0;
        private int[] PosicaoCavalos { get; }
        public static int TamanhoDaCorrida { get; set; }
        public static int DistanciaMaximaEntreCavalos { get; set; }
        Cavalo _cavalo;

        public Corrida(int[] posicaoDeCavalos, Cavalo cavalo)
        {
            PosicaoCavalos = posicaoDeCavalos;
            _cavalo = cavalo;
        }


        public void QueComeceACorrida()
        {

            int minhaPosicao;
            bool cheguei = false;
            do
            {
                if (new Random().Next(100) % 2 == 0)
                    Thread.Yield();

                minhaPosicao = PosicaoCavalos[_cavalo.Numero];

                lock (PosicaoCavalos)
                {
                    bool cavaloMuitoNaDianteira = false;
                    foreach (var p in PosicaoCavalos)
                    {
                        if (Math.Abs((minhaPosicao + _cavalo.Velocidade) - p) >= DistanciaMaximaEntreCavalos)
                        {
                            cavaloMuitoNaDianteira = true;
                            break;
                        }
                    }

                    if (!cavaloMuitoNaDianteira)
                    {
                        PosicaoCavalos[_cavalo.Numero] += _cavalo.Velocidade;
                        minhaPosicao = PosicaoCavalos[_cavalo.Numero];
                        Console.WriteLine($"{_cavalo.Numero} ==> {minhaPosicao} metros");

                        cheguei = minhaPosicao >= TamanhoDaCorrida;
                        if (cheguei)
                        {
                            _colocacao++;
                            Console.WriteLine($"{_cavalo.Numero} ==> Cheguei em {_colocacao}º");

                        }
                    }
                }


            } while (!cheguei);
        }
    }
}
