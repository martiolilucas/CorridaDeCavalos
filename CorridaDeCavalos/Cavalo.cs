﻿using System;

namespace CorridaDeCavalos
{
    class Cavalo
    {
        private static int _internCount = 0;
        public int Velocidade { get; set; }
        public int Numero { get; set; }

        public Cavalo()
        {
            Numero = _internCount;
            ++_internCount;
            if (Numero % 2 != 0)
                Velocidade = new Random().Next(1, 5);
            else
                Velocidade = new Random().Next(1, 3);
        }

    }
}
